// jackson.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

struct Proces
{
	int r; // termin dostępnosci
	int p; // czas obsługi
};

vector <Proces> procesy;

bool wczytaj(string nazwa, int &ilosc)
{
	Proces proces;
	ifstream plik;
	plik.open(nazwa.c_str());
	if (plik.good())
		cout << "Udalo sie otworzyc plik" << endl;
	else
	{
		cout << "Nie udalo sie otworzyc pliku" << endl;
		return false;
	}

	plik >> ilosc;

	while (!plik.eof())
	{
		plik >> proces.r >> proces.p;
		procesy.push_back(proces);
	}

}
int porownaj(int a, int b)  // funkcja pomocnicza
{
	if (a>b)
		return a;
	else return b;
}


void wyswietl(int ilosc)             //wyswietlanie wczytanych danych
{
	cout << ilosc << endl;
	for (int i = 0; i<procesy.size(); i++)
	{
		cout << procesy[i].r << " " << procesy[i].p << endl;
	}
}

void wyniki(string nazwa)       // Funkcja wyswietlajaca wyniki dla odpowiednich danych wejsciowych
{
	if (nazwa[4] == '1')
	{
		cout << "Wynik: 31" << endl;
	}
	if (nazwa[4] == '2')
	{
		cout << "Wynik: 580" << endl;
	}
	if (nazwa[4] == '3')
	{
		cout << "Wynik: 907" << endl;
	}
	if (nazwa[4] == '4')
	{
		cout << "Wynik: 1006" << endl;
	}
	if (nazwa[4] == '5')
	{
		cout << "Wynik: 2355" << endl;
	}
	if (nazwa[4] == '6')
	{
		cout << "Wynik: 2586" << endl;
	}
	if (nazwa[4] == '7')
	{
		cout << "Wynik: 4942" << endl;
	}
	if (nazwa[4] == '8')
	{
		cout << "Wynik: 5042" << endl;
	}
}

void b_sort(int ile_liczb)
{
	int temp, i, zmiana;
	do
	{
		zmiana = 0;
		i = ile_liczb - 1;
		do
		{
			i--;
			if (procesy[i + 1].r < procesy[i].r)
			{
				swap(procesy[i].r, procesy[i + 1].r);
				swap(procesy[i].p, procesy[i + 1].p);
				zmiana = 1;
			}
		} while (i != 0);
	} while (zmiana != 0);
}

int Cmax()  // obliczanie funkcji celu
{
	int C=0;
	
	for (int i = 0; i < procesy.size(); i++)
	{
		C = max(procesy[i].r, C) + procesy[i].p;
	}

	return C;
}


int main()
{
	string nazwa;
	int ilosc;

	cout << "Podaj nazwe pliku" << endl;
	cin >> nazwa;
	nazwa += ".DAT";
	cout << "Wczytano plik :" << nazwa << endl;
	wczytaj(nazwa, ilosc);
	//wyswietl(ilosc);
	wyniki(nazwa);

	b_sort(procesy.size());

	//wyswietl(ilosc);

	cout << "Obliczony wynik: " << Cmax() << endl;

	system("pause");

    return 0;
}

